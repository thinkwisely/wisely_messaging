# -*- coding: utf-8 -*-
# ===============LICENSE_START=======================================================
# MIT License
#
# Copyright (c) 2019 wisely.io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# ===============LICENSE_END=========================================================
from os.path import dirname, abspath, join as path_join
from json import load as load_json
from setuptools import setup, find_packages


SETUP_DIR = abspath(dirname(__file__))
DOCS_DIR = path_join(SETUP_DIR, 'docs')
ASSET_DIR = path_join(SETUP_DIR, 'wisely_messaging', 'assets')


def _long_descr():
    '''Yields the content of documentation files for the long description'''
    for filen in ('readme.rst',):
        doc_path = path_join(DOCS_DIR, filen)
        with open(doc_path) as f:
            yield f.read()


with open(path_join(ASSET_DIR, 'version.json')) as fileh:
    globals_dict = load_json(fileh)


setup(
    author='Eric Zavesky',
    author_email='eric@thinkwisely.io',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'License :: OSI Approved :: MIT',
    ],
    description=globals_dict["description"],
    install_requires=['gunicorn', 'gevent', 'flask', 'hiredis', 'connexion', 'redis'],
    package_data={globals_dict["name"]: [path_join('assets', "*.*")]},
    entry_points={
        'console_scripts': [
            f'{globals_dict["name"]} = wisely_messaging.server:app'
        ],
    },
    include_package_data=True,
    keywords='wisely messaging redis replay logging processing',
    license='MIT',
    long_description='\n'.join(_long_descr()),
    name=globals_dict["name"],
    packages=find_packages(),
    python_requires='>=3.6',
    url='https://gitlab.com/thinkwisely/wisely_messaging.git',
    version=globals_dict["version"],
)
