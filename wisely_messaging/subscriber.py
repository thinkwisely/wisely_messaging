#! python
################################################################################
# MIT License
#
# Copyright (c) 2019 wisely.io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
################################################################################
# -*- coding: utf-8 -*-

import json
import logging
from datetime import datetime, timedelta
from os.path import dirname, abspath
from queue import Queue
# from functools import partial
import re

from wisely_messaging import publisher

ROOT = dirname(abspath(__file__))

CHANNEL_METADATA = "metadata"
CHANNEL_SPEECH = "speech"
CHANNEL_TEXT = "text"
CHANNEL_SCORE = "text"
CHANNEL_ARTICLE = "article"
CHANNEL_IMAGE = "image"
CHANNEL_TOPIC = "topic"

CLEANUP_SUBSCRIBER = 5 * 60  # time in seconds to troll fo runused Queues

logger = logging.getLogger(__name__)
re_channel = re.compile(publisher.CHANNEL_PAYLOAD + ".*")


class RedisSubscriber():
    """Primary message subscriber for medis messages
       and returning results on the same data channel using  `Redis <https://redis.io/>`_.
    """

    def __init__(self, redis_instance, func_callback, suffix_media, has_payload=True, single_channel=False):
        """Initialize a subscriber for media+payload channel messages

        Args:
            redis_instance (redis.Redis): Previously created `Redis Instance <https://redis-py.readthedocs.io/en/latest/>_`
            func_callback (func): Function to be called when valid media chunks are received (see `process_media_payload`_ for template)
            suffix_media (str): Suffix for data payload for this media
            has_payload (bool): Does this channel have a payload associate with it?
            single_channel (bool): Do we include a psubscribe (multi channel with asterisk) or not?
        """

        self.redis = redis_instance
        self.channel_queue = {}
        self.timestamp_clean = datetime.now()
        self.callback = func_callback

        # open subscription channels for incoming media
        self.sub_media = self.redis.r.pubsub(ignore_subscribe_messages=True)
        self.sub_media_thread = None
        self.sub_media_payload = None
        self.sub_media_suffix = suffix_media
        sub_chan = self.sub_media_suffix if single_channel else publisher.RedisPublisher.channel_name("*", self.sub_media_suffix)

        logger.info(f"Subscribe media listener to '{sub_chan}'")
        # print(f"** Subscribe media listener to '{sub_chan}'")
        if func_callback is None:  # launch thread if callback provided
            self.sub_media.psubscribe(sub_chan)
        else:
            self.sub_media.psubscribe(**{sub_chan: self.process_media_threaded})  # partial(process_audio_threaded, self)} )
            # use some automagic thread love - https://github.com/nicois/redis-py#publish--subscribe
            self.sub_media_thread = self.sub_media.run_in_thread(sleep_time=0.001)

        if has_payload:
            # create subscription for payload, too
            self.sub_media_payload = self.redis.r.pubsub(ignore_subscribe_messages=True)
            channel_payload = publisher.RedisPublisher.channel_name("*", f"{self.sub_media_suffix}{publisher.CHANNEL_PAYLOAD}")
            logger.info(f"Subscribe metadata listener to '{channel_payload}'")
            self.sub_media_payload.psubscribe(channel_payload)

    def __del__(self):
        """Pythonic shutdown, which won't hurt if other instances also shutdown"""
        self.shutdown()

    def __exit__(self):
        """Pythonic shutdown, which won't hurt if other instances also shutdown"""
        self.shutdown()

    def shutdown(self):
        """Shutdown and terminate any listening (subscription) threads and instances"""
        if self.sub_media_thread:  # stop the thread
            self.sub_media_thread.stop()
        # close other pub/sub models
        # if self.sub_media is not None:
        #     self.sub_media.close()
        #     self.sub_media = None
        # if self.sub_media_payload is not None:
        #     self.sub_media_payload.close()
        #     self.sub_media_payload = None

    def process_media_bulk(self):
        """Process media by reading as many messages as possible from the channel.  Also pull payload data
        if that component was enabled.  Otherwise second part of return tuple will always be None.

        Returns:
            array: array of decoded redis messages (and payload) (not json decoded) [(data, payload), ...]
        """
        list_messages = []
        if self.sub_media is not None:
            # print(f"** inside bulk")
            redis_msg = self.sub_media.get_message()
            while redis_msg is not None:
                # print(f"** inside message ")
                if redis_msg["data"] and type(redis_msg["data"]) == bytes:
                    channel_query = redis_msg['channel'].decode("utf-8")
                    data_payload = self._process_payload_single(channel_query)
                    list_messages.append((redis_msg["data"].decode("utf-8"), data_payload))
                redis_msg = self.sub_media.get_message()
        return list_messages

    def _process_payload_single(self, channel_query):
        """Internal function to pull the relevant payload for a channel, will block/wait for payload

        Args:
            channel_query (str): the channel to match for payload

        Returns:
            message_data: either bytes or string from payload (None on error)
        """
        # periodic clean up
        if (self.timestamp_clean + timedelta(seconds=CLEANUP_SUBSCRIBER)) < datetime.now():
            # print(f"** inside clean")
            for chan in list(self.channel_queue.keys()):
                if self.channel_queue[chan].empty():
                    del self.channel_queue[chan]
            self.timestamp_clean = datetime.now()

        # pull info required for this triggering chunk
        data_payload = None
        if self.sub_media_payload is not None:  # only if looking for payload, too
            while data_payload is None:  # loop until we found payload chunk
                # print(f"** inside payload loop")
                if channel_query in self.channel_queue and not self.channel_queue[channel_query].empty():
                    data_payload = self.channel_queue[channel_query].get()
                else:
                    redis_data_msg = self.sub_media_payload.get_message()
                    if redis_data_msg:
                        channel_data = re_channel.sub("", redis_data_msg['channel'].decode("utf-8"))
                        if channel_query != channel_data:  # got someone else's data, so queue it
                            if channel_data not in self.channel_queue:
                                self.channel_queue[channel_data] = Queue()
                            self.channel_queue[channel_data].put(redis_data_msg["data"])
                        else:
                            data_payload = redis_data_msg["data"]
        return data_payload

    def process_media_threaded(self, redis_msg):
        """Method to process incoming media messages.  Note that this message first indicates that
        another payload is in queue, which will contain only byte-based content

        Args:
            redis_msg (redis.Redis): Redis message `Redis Instance <https://redis-py.readthedocs.io/en/latest/>_`
        """

        # pull info required for this triggering chunk
        channel_query = redis_msg['channel'].decode("utf-8")
        data_payload = self._process_payload_single(channel_query)

        data_channel = None
        if redis_msg["data"] and type(redis_msg["data"]) == bytes:
            data_channel = json.loads(redis_msg['data'].decode("utf-8"))
        if self.callback is not None:
            self.callback(channel_query, data_channel, data_payload)
        else:
            self.process_media_payload(channel_query, data_channel, data_payload)
        logger.debug(redis_msg)

    def process_media_payload(self, channel_query, data_channel, data_payload):
        """(template) Method to process incoming media messages.  Note that this message first indicates that
        another payload is in queue, which will contain only byte-based content.

        Args:
            channel_query (str): Specific channel for data and payload prefix
            data_channel (dict): Dictionary of channel publish data from this message
            data_payload (bytes): Bytes for encoded payload data
        """
        logger.debug(f"Default media processing routine called: {channel_query}, {data_channel}")
