#! python
################################################################################
# MIT License
#
# Copyright (c) 2019 wisely.io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
################################################################################
# -*- coding: utf-8 -*-

# import redis
# from os.path import join as path_join
import json
from datetime import datetime

import logging
logger = logging.getLogger(__name__)

TIMESTAMP_FORMAT = "%Y%m%d_%H%M%S_%f"
EOS_TRIGGER = "EOS"
AUDIO_RATE = 16000

CHANNEL_PAYLOAD = ".payload"
CHANNEL_PARTITION = "::"

DICT_META_NEW_UTTERANCE = {"type": "utterance", "desc": "New Utterance Detected"}
DICT_META_NEW_RECOGNITION = {"type": "recognition", "desc": "New Recognition Completed"}
DICT_META_NEW_SCORING = {"type": "score", "desc": "New Scoring Detected"}
DICT_META_NEW_HISTORY = {"type": "historical", "desc": "New Historical Discussion Discovered"}
DICT_META_NEW_ARTICLE = {"type": "article", "desc": "New Article Discussion Linked"}
DICT_META_NEW_TOPIC = {"type": "topic", "desc": "New Topic Expansion Created"}


def timestamp_now():
    return datetime.now().strftime(TIMESTAMP_FORMAT)


class RedisPublisher:
    """Base callback class publishing chunks to a redis stream

    Provides a callback function for stream writer/transform to write chunks of data.
    """

    def __init__(self, redis_instance, channel_data=None, channel_meta=None):
        """Initialize publisher with instance and channel.

        Args:
            redis_instance (redis.Redis): Previously created `Redis Instance <https://redis-py.readthedocs.io/en/latest/>_`
            channel_data (str): Base name for channel to be created for data transmission
            channel_meta (str): Base name for channel to be created for meta transmission
        """
        self.redis = redis_instance
        self.channel_data = channel_data
        self.channel_meta = channel_meta
        if self.channel_data is not None:
            logger.info(f"Subscribe publisher to '{channel_data}' and '{channel_meta}'")
        else:
            logger.info(f"Publisher created but has no specific channel")

    def media_publish(self, samples, pts, duration, dict_meta=None, stats={}, rate=1, timestamp=None):
        """Push a new message for incoming data.  At writing, three channels are published to:
            X-data (metadata for frame), X-data-payload (bytes), X-meta (client-metadata).

         Args:
            samples (bytes or str): str or byte data array of media samples
            pts (float): presentation time stamp for this media stream
            duration (float): part of a second for this frame (e.g. duration)
            dict_meta (dict): dict or None to be set when media is marked as 'complete' (e.g. a whole utterance, recognition, etc)
            stats (dict): dict with specialized media data (e.g. 'voiced' and 'unvoiced' in millis for audio)
            rate (int): sample rate for this media sample
            timestamp (str): timestamp for start of this media (None=now)

        Returns:
            nd_array: None
        """
        # cascade call with named channel specification
        return self.media_publish_channel(samples, pts, duration, dict_meta, self.channel_data, stats, rate, timestamp, self.channel_meta)

    def media_publish_channel(self, samples, pts, duration, dict_meta, channel_data, stats={}, rate=1, timestamp=None, channel_meta=None):
        """Push a new message for incoming data with explicitly named channels.  At writing, three channels are published to:
            X-data (metadata for frame), X-data-payload (bytes), X-meta (client-metadata).

         Args:
            samples (bytes or str): str or byte data array of media samples
            pts (float): presentation time stamp for this media stream
            duration (float): part of a second for this frame (e.g. duration)
            dict_meta (dict): dict or None to be set when media is marked as 'complete' (e.g. a whole utterance, recognition, etc)
            stats (dict): dict with specialized media data (e.g. 'voiced' and 'unvoiced' in millis for audio)
            rate (int): sample rate for this media sample
            channel_data (str): Base name for channel to be created for data transmission
            timestamp (str): timestamp for start of this media
            channel_meta (str): Base name for channel to be created for meta transmission (optional)

        Returns:
            nd_array: None
        """
        if channel_data is None:
            return None
        if timestamp is None:
            timestamp = timestamp_now()
        msg_base = {"timestamp": timestamp, "stats": stats}
        msg_data = dict(msg_base)  # make a copy so we can edit it
        msg_data.update({"pts": pts, "rate": rate, "is_active": 0 if dict_meta is None else 1, "duration": duration})
        # print(f"** Push msg {msg_data} to channel {channel_data}")
        try:
            self.redis.r.publish(channel_data, json.dumps(msg_data))
            self.redis.r.publish(f"{channel_data}{CHANNEL_PAYLOAD}", samples)
            if channel_meta is not None and dict_meta is not None:
                msg_base.update(dict_meta)
                # print(f"** Push meta {msg_base} to channel {channel_meta}")
                self.redis.r.publish(channel_meta, json.dumps(msg_base))
        except Exception as e:
            logger.warning(f"Failed to send data on channel {channel_data}, meta {channel_meta}; {e}")
        return None

    @staticmethod
    def channel_name(channel_base, type_suffix=None):
        """Create a new channel name from raw channel or pre-created channel.  A simple, but consisent
            naming methodology is enforced for utility.

        Args:
            channel_base (str): str inpout name to be bcleaned
            type_suffix (str): str suffix to be appended for final name (None=pure channel)

        Returns:
            str: A string name for the requested channel.
        """
        channel_base = channel_base.split(CHANNEL_PARTITION)[0]
        if type_suffix is not None:
            return CHANNEL_PARTITION.join([channel_base, type_suffix])
        return channel_base
