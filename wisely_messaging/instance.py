#! python
################################################################################
# MIT License
#
# Copyright (c) 2019 wisely.io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
################################################################################
# -*- coding: utf-8 -*-

import redis

import logging
logger = logging.getLogger(__name__)


class RedisInstance():
    """Base redis stream instance

    Provides basic instance from host and server
    """

    def __init__(self, host, port):
        """Initialize publisher with instance and channel.

        Args:
            host (str): Base host name or IP
            port (int): Host port for connection
        """
        self._r = redis.Redis(host=host, port=int(port))

    @property
    def r(self):
        return self._r

    def expire_keys(self):
        """Under development, but a method to clear out old keys that need to be expired"""
        # https://stackoverflow.com/a/26033818
        # for key in redis.keys('*'):
        for key, value in self.r.scan_iter():
            if self.r.ttl(key) == -1:
                self.r.expire(key, 60 * 60 * 24 * 7)

    # TODO: record method
    # TODO: playback method
    # TODO: timer for recording
