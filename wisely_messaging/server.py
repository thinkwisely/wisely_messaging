#! python
################################################################################
# MIT License
#
# Copyright (c) 2019 wisely.io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
################################################################################
# -*- coding: utf-8 -*-

import argparse
import logging
import sys
from os.path import dirname, abspath, join as path_join, exists
from os import makedirs

# from connexion import FlaskApp
from flask import request, render_template, send_from_directory, Flask
import atexit

ROOT = dirname(abspath(__file__))

if __name__ == '__main__':
    # patch the path to include this object
    if ROOT not in sys.path:
        sys.path.append(ROOT)

# from wisely_messaging import *

logger = logging.getLogger(__name__)


def create_app(argv=sys.argv[1:]):
    parser = argparse.ArgumentParser(
        description="Wisely Messaging Server"
    )
    parser.add_argument("-p", "--port", type=int, default=8080, help="Port for HTTP server (default: 8080)")
    parser.add_argument("-d", "--data_dir", type=str, default="data", required=False,
                        help="Location of where to save/load messages to store")
    parser.add_argument("--verbose", "-v", action="count")
    subparse = parser.add_argument_group('redis server configuration')
    subparse.add_argument("-S", "--redis_server", dest='redis_server',
                          default=None, type=str, help="server or host for redis")
    subparse.add_argument("-P", "--redis_port", dest='redis_port',
                          default=6379, type=int, help="port for redis connectivity")
    subparse.add_argument("-s", "--suffix_monitor", nargs='+', dest='suffix_monitor',
                          help="topic suffix for subscribing to pub/sub data")

    run_settings = vars(parser.parse_args(argv))
    logger.info(f"Run Settings: {run_settings}")

    if not exists(run_settings["data_dir"]):
        makedirs(run_settings["data_dir"])
        if not exists(run_settings["data_dir"]):
            logger.fatal(f"Attempt to create {run_settings['data_dir']} failed, please check filesystem permissions")
            return None

    if run_settings['verbose']:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    app = Flask(__name__, template_folder=path_join(ROOT, "assets"))
    # app = cxn_app.app
    app.settings = run_settings
    app.status = {}

    @app.route('/')
    def root():
        config_payload = {}
        config_payload.update(app.settings)
        config_payload.update(app.status)
        return render_template('index.html', **config_payload)  # inject all args

    @app.route('/<path:path>/')
    def render_static(path):
        return send_from_directory(path_join(ROOT, "assets"), path)

    @app.route('/transact_file', methods=['POST'])
    def recognize_file():
        enhanced = request.values.get('enhanced', default=1, type=int)
        enhanced = enhanced is not False and int(enhanced) != 0  # input normalize
        data_file = request.files.get('file')
        data = None  # MessageServer.data_resample(data_file.stream) if data_file is not None else None
        logger.info(f"recognize: file: {'(missing file)' if not data_file else data_file.filename}, " +
                    f"len: {0 if not data_file else data_file.content_length}, " +
                    f"len-audio: {0 if not data else len(data)}, enhanced: {enhanced}")
        return_str = ""
        return return_str

    def close_running_threads():    # defining function to run on shutdown
        pass

    # Register the function to be called on exit
    atexit.register(close_running_threads)

    # app.on_shutdown.append(on_shutdown)
    return app


# Gunicorn entry point generator, https://stackoverflow.com/a/46333363
def app(*args, **kwargs):
    # Gunicorn CLI args are useless.
    # https://stackoverflow.com/questions/8495367/
    #
    # Start the application in modified environment.
    # https://stackoverflow.com/questions/18668947/
    #
    argv = list(args)
    for k in kwargs:
        argv.append("--" + k)
        argv.append(kwargs[k])
    logger.info(f"App Settings: {argv}")
    return create_app(argv)


def main():
    app_inst = create_app()
    app_inst.run(host='0.0.0.0', port=app_inst.settings['port'])


if __name__ == "__main__":
    main()
