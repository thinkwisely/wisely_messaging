# This file is a template, and might need editing before it works on your project.
FROM conda/miniconda3:latest

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
# RUN apk --no-cache add postgresql-client

# default redis properties from build
ARG redis_host_param=localhost  
ARG redis_port_param=6379
# environment variables that can get overwritten at runtime
ENV redis_host=$redis_host_param
ENV redis_port=$redis_port_param

WORKDIR /usr/src/app

COPY . /tmp/app

RUN apt-get update \ 
    # && apt-get install -y gcc libgl1-mesa-glx \  # USE THIS IF OPENCV IS REQUIRED
    # temporary while we need to build aiortc wheel
    # && conda install av netifaces libopus libvpx opencv -c conda-forge  \  # USE THIS IF OPENCV IS REQUIRED
    # && conda install av netifaces libopus libvpx -c conda-forge  \
    # 'av' will also include ffmpeg as an install dependency
    && pip install --no-cache-dir /tmp/app \
    # clean up mess from gcc
    # && apt-get -qq -y remove gcc \
    && apt-get -qq -y autoremove \
    && apt-get autoclean \
    # clean up temp app
    && rm -rf /tmp/app

# copy the certificate from public destination

# get target server for where to send audio (e.g. redis or volume)


# For webserver
EXPOSE 8080
CMD gunicorn -k gevent --workers=1 --bind=0.0.0.0:8080 -t 90 "wisely_messaging.server:app(redis_server='$redis_host',redis_port='$redis_port')"
# CMD wisely_messaging --port 8080 -S $redis_host -P $redis_port

# For some other command
# CMD ["python", "app.py"]

