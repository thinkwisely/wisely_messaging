#! python
################################################################################
# MIT License
#
# Copyright (c) 2019 wisely.io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
################################################################################
# -*- coding: utf-8 -*-

import pytest
import os
import uuid
import logging
from datetime import datetime
from time import sleep
from functools import partial
import copy

import wisely_messaging
from wisely_messaging import instance, subscriber, publisher

logger = logging.getLogger(__name__)

REDIS_HOST = os.environ["REDIS_HOST"]
REDIS_PORT = os.environ["REDIS_PORT"]
run_stats_template = {"seen": 0, "channels": [], "data": [], "stat": [], "time": [], "payload": []}


def process_media_payload(run_log, channel_query, data_channel, data_payload):
    """Test function for receiving payload"""
    logger.info(f"Default media processing routine called: {channel_query}, {data_channel}")
    # print(f"** in callback - {channel_query}:{data_channel}")
    run_log["channels"].append(channel_query)
    run_log["data"].append(data_channel)
    run_log["payload"].append(data_payload)
    run_log["time"].append(datetime.now())


def safe_wait(time_wait=0.5):
    """Sleep function for data to route through redis instance"""
    # print(f"** sleeping")
    sleep(time_wait)


def clean_runlog():
    run_media = copy.deepcopy(run_stats_template)
    run_meta = copy.deepcopy(run_stats_template)
    return run_media, run_meta


def test_version():
    """Simple version test for coverage"""
    version_info = wisely_messaging.version()
    assert version_info['name'] == "wisely_messaging"


@pytest.mark.timeout(60)
def test_bulk_sub():
    print(f"test with no callback in a subscriber redis instance at {REDIS_HOST}:{REDIS_PORT}")

    # test bad connection
    with pytest.raises(TypeError):
        run_inst = instance.RedisInstance(host="bad host", port=None)
    with pytest.raises(ValueError):
        run_inst = instance.RedisInstance(host="bad host", port="")
    # Test good connection
    run_inst = instance.RedisInstance(host=REDIS_HOST, port=REDIS_PORT)
    # attempt to listen
    run_media, run_meta = clean_runlog()
    sub_speech = subscriber.RedisSubscriber(run_inst, partial(process_media_payload, run_media), subscriber.CHANNEL_SPEECH, False)  # test with callabck func, but no payload
    sub_speech2 = subscriber.RedisSubscriber(run_inst, None, subscriber.CHANNEL_SPEECH, False)  # test with payload callabck
    # create a channel for speech
    pc_uuid = str(uuid.uuid4())
    chan_aud = publisher.RedisPublisher.channel_name(pc_uuid, subscriber.CHANNEL_SPEECH)
    chan_meta = publisher.RedisPublisher.channel_name(pc_uuid, subscriber.CHANNEL_METADATA)
    print(f"channel {chan_aud}, meta {chan_meta}")
    pub_speech = publisher.RedisPublisher(run_inst, chan_aud, chan_meta)  # publisher fixed on these channels
    # channel specific?
    sub_speech3 = subscriber.RedisSubscriber(run_inst, None, subscriber.CHANNEL_SPEECH, False, True)  # test with no callabck, single (wrong) channel
    sub_speech4 = subscriber.RedisSubscriber(run_inst, None, chan_aud, False, True)  # test with no callabck, single (correct) chanel
    safe_wait()
    assert len(run_media["data"]) == 0  # no messages should have come yet
    assert len(run_meta["data"]) == 0  # no messages should have come yet

    msg_list = sub_speech.process_media_bulk()  # should still be able to get messages this way
    assert len(msg_list) == 0  # shouldn't be anything there
    msg_list = sub_speech2.process_media_bulk()  # should still be able to get messages this way
    assert len(msg_list) == 0  # shouldn't be anything there

    msg_list = sub_speech3.process_media_bulk()  # should still be able to get messages this way
    assert len(msg_list) == 0  # specific channel, didn't catch all
    msg_list = sub_speech4.process_media_bulk()  # was watching he same channel
    assert len(msg_list) == 0  # specific channel, nothing there

    num_msg = 5
    for idx in range(num_msg):
        dtn = datetime.now()
        pub_speech.media_publish(b"This is a test", dtn.microsecond, dtn.second, None, {"test": idx})
    safe_wait()
    assert len(run_media["data"]) == num_msg  # validate that we've gotten a test message
    assert len(run_meta["data"]) == 0  # no messages should have come yet

    msg_list = sub_speech3.process_media_bulk()  # should still be able to get messages this way
    assert len(msg_list) == 0  # specific channel, didn't catch any
    msg_list = sub_speech4.process_media_bulk()  # was watching he same channel
    assert len(msg_list) == num_msg  # specific channel, caught the one

    msg_list = sub_speech.process_media_bulk()  # should still be able to get messages this way
    assert len(msg_list) == 0  # already fetched from thread (callback)
    msg_list = sub_speech2.process_media_bulk()  # was watching the same channel
    assert len(run_media["data"]) == len(msg_list)  # should be the same!

    sub_speech.shutdown()  # don't forget to shutdown the thread!


@pytest.mark.timeout(60)
def test_sub_payloads():
    print(f"test with valid callback+payload in a subscriber redis instance at {REDIS_HOST}:{REDIS_PORT}")

    # Test good connection
    run_inst = instance.RedisInstance(host=REDIS_HOST, port=REDIS_PORT)
    # attempt to listen
    run_media, run_meta = clean_runlog()
    sub_speech = subscriber.RedisSubscriber(run_inst, partial(process_media_payload, run_media), subscriber.CHANNEL_SPEECH, True)  # test with payload callabck
    sub_meta = subscriber.RedisSubscriber(run_inst, partial(process_media_payload, run_meta), subscriber.CHANNEL_METADATA, False)  # test with payload callabck
    # create a channel for speech
    pc_uuid = str(uuid.uuid4())
    chan_aud = publisher.RedisPublisher.channel_name(pc_uuid, subscriber.CHANNEL_SPEECH)
    chan_meta = publisher.RedisPublisher.channel_name(pc_uuid, subscriber.CHANNEL_METADATA)
    pub_speech = publisher.RedisPublisher(run_inst, chan_aud, chan_meta)  # publisher fixed on these channels
    # pub_recognition = publisher.RedisPublisher(run_inst)  # publisher not fixed on a channel
    safe_wait()
    assert len(run_media["data"]) == 0  # no messages should have come yet
    assert len(run_meta["data"]) == 0  # no messages should have come yet
    msg_list = sub_meta.process_media_bulk()  # no messages through pull either!
    assert len(msg_list) == 0  # still clean, right?!

    dict_data = b"This is a test"
    dict_stat = {"test": 1}
    pub_speech.media_publish(dict_data, 1.1234, 0.445, None, dict_stat)
    safe_wait()
    assert len(run_media["data"]) == 1  # validate that we've gotten a test message
    assert len(run_meta["data"]) == 0  # no messages should have come yet

    assert run_media["data"][0]["stats"] == dict_stat
    assert run_media["payload"][0] == dict_data
    assert len(run_media["payload"]) == 1

    dict_meta = {"other": 2}
    dict_stat = {"test": 2}
    pub_speech.media_publish(dict_data, 1.1234, 0.445, dict_meta, dict_stat)  # should publish to meta, too
    safe_wait()
    assert len(run_media["data"]) == 2  # validate that we've gotten a test message
    assert len(run_meta["data"]) == 1  # no messages should have come yet
    msg_list = sub_meta.process_media_bulk()  # no messages through pull either!
    assert len(msg_list) == 0  # still clean, right?!

    # print(f"** {run_meta}")
    assert run_media["data"][1]["stats"] == dict_stat
    assert run_meta["payload"][0] is None
    for k in dict_meta:
        assert k in run_meta["data"][0] and run_meta["data"][0][k] == dict_meta[k]

    sub_speech.shutdown()
    sub_meta.shutdown()
