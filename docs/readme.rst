.. ===============LICENSE_START=======================================================
.. MIT License
.. ===================================================================================
.. Copyright (c) 2019 thinkwisely
.. ===================================================================================
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. ===================================================================================
.. The above copyright notice and this permission notice shall be included in all
.. copies or substantial portions of the Software.
.. ===================================================================================
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.
.. ===============LICENSE_END=========================================================

========
Overview
========

A server library that provides a light messaging wrapper around redis
for pub/sub operations.  It also provides functionality for logging and replaying
redis message content. View the `full documentation <https://thinkwisely.gitlab.io/wisely_server>`_ 
and coverage information for more details.

Getting Started
===============

Runtime Requirements
--------------------

For communication with other modules, be sure to start and associate a Redis
instance.  At the time of writing, this server should be password free and 
it does not require certificates.

On easy way to launch it with docker is thus, where the port (`-P`) and host (`-S`)
settings of the wisely server need to be configured to match this instance.

.. code:: bash
    
    docker run --rm -d -p 6379:6379 redis



Datachannel Semantics
=====================

TBD

.. code:: bash

    TBD



Coverage Record
===============

`Coverage reports <_static/index.html>`_ are included for this version of the server.

