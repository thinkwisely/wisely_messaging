.. ===============LICENSE_START=======================================================
.. MIT License
.. ===================================================================================
.. Copyright (c) 2019 thinkwisely
.. ===================================================================================
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. ===================================================================================
.. The above copyright notice and this permission notice shall be included in all
.. copies or substantial portions of the Software.
.. ===================================================================================
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.
.. ===============LICENSE_END=========================================================

=============
Release Notes
=============

v0.1
====

0.1.3
-----

- Normalize channel name creation for suffixes; defer to packaging implementation
- Add new channels for scoring, links, articles, topics

0.1.2
-----

-  Fix suffix creation for subscriber notices


0.1.1
-----

-  Update after additional testing for gunicorn and flask serving


0.1.0
-----

-  Basic server creation

    - Redis-based pub/sub message analysis
    - Base classes for message creation and monitoring
    - Record and replay of redis messages to/from disk
