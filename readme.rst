.. MIT License

.. Copyright (c) 2019 thinkwisely

.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:

.. The above copyright notice and this permission notice shall be included in all
.. copies or substantial portions of the Software.

.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.


================
wisely_messaging
================

|Build Status| |Coverage|

A server library that receives media connections for processing.

See the `live documentation <https://thinkwisely.gitlab.io/wisely_messaging>`_ or the
`raw usage documentation <docs/readme.rst>`_ to get started.


.. |Build Status| image:: https://gitlab.com/thinkwisely/wisely_messaging/badges/master/pipeline.svg
   :target: https://gitlab.com/thinkwisely/wisely_messaging/commits/master

.. |Coverage| image:: https://gitlab.com/thinkwisely/wisely_messaging/badges/master/coverage.svg
   :target: https://gitlab.com/thinkwisely/wisely_messaging/commits/master
